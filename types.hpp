#ifndef _TYPES_HPP_
#define _TYPES_HPP_

/* class to represent null types */
class NullType { /* ... */ };

/* converting integral type to class type */
template<int v>
struct Int2Type
{
  enum { value = v };
};

/* template class to check convertion */
template<class T, class U>
class CanConvert
{
  typedef char Yes;
  struct No { char dummy[2]; };
  
  static Yes Check(U);
  static No  Check(...);
  
  static T MakeT();

public:
  enum { value = (sizeof(Yes) == sizeof(Check(MakeT()))) };
};

/* policy uses either clone or copy */
template<class T, bool isPoly>
class CopyPolicy
{
  
  static T* Copy(T* t, Int2Type<true>)
  {
    return t->Clone();
  }

  static T* Copy(T* t, Int2Type<false>)
  {
    return new T(*t);
  }
public:
  static T* Copy(T* t)
  {
    return Copy(t, Int2Type<isPoly>());
  }
};

/* compile time type info */
template<typename T>
class TypeInfo
{
  template<class U> struct PointerTraits
  {
    enum { result = false };
    typedef NullType PointeeType;
  };

  template<class U> struct PointerTraits<U*>
  {
    enum { result = true };
    typedef U PointeeType;
  };
public:
  enum { isPointer = PointerTraits<T>::result };
  typedef typename PointerTraits<T>::PointeeType Type;
};

#endif /* _TYPES_HPP_ */
