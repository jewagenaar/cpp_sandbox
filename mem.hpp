#ifndef _MEM_HPP_
#define _MEM_HPP_

#include <stdlib.h>
#include <vector>
#include <map>
#include <assert.h>

/**
 * Memory chunk with embedded singly linked list. Reserves fixed number
 * of blocks of a specified size.
 */
struct Chunk
{
  // init the chunk
  void Init (size_t blocksize, unsigned char blocks);
  // allocate a new small object in this chunk
  void* Alloc (size_t blocksize);
  // deallocate the memory of the small object
  void Dealloc (void *p, size_t blocksize); 

  // pointer to the start of the data
  unsigned char* data;
  // the first available data loc, and the num available
  unsigned char firstAvailable, numAvailable;
};

/**
 * Fixed size allocator. Keeps a std::vector<Chunk> of uniform size.
 */
class FixedAllocator
{
  typedef std::vector<Chunk> Chunks;

public:
  FixedAllocator(size_t blocksize, unsigned char blocks);

  void* Alloc   (void);
  void  Dealloc (void *p);

private:
  size_t        m_Blocksize;
  unsigned char m_Blocks;
  unsigned char m_EmptyChunks;

  Chunk* m_LastAlloc;
  Chunk* m_LastDealloc;
  Chunks m_Chunks;
};

/**
 * Allocates memory for small objects.
 */
class SmallObjectAllocator
{
public:
  SmallObjectAllocator (size_t maxSize = 4096, unsigned char blocks = 255);

  void* Alloc (size_t s);
  void  Dealloc (void *p, size_t s);
private:
  size_t        m_MaxSize;
  unsigned char m_Blocks;
  std::map<size_t, FixedAllocator*> m_Pool; 
};

/**
 * Small object.
 */
class SmallObject 
{
public:
  static void* operator new    (size_t s);
  static void  operator delete (void *p, size_t s);

  virtual ~SmallObject (void) {};
}; 

#endif // _MEM_HPP_
