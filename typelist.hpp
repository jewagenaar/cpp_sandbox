#ifndef _TYPELIST_HPP_
#define _TYPELIST_HPP_

#include "types.hpp"

template<class U, class T>
struct TypeList
{
  typedef U Head;
  typedef T Tail;
};

#define TYPELIST1(T1) TypeList<T1, NullType>
#define TYPELIST2(T1, T2) TypeList<T1, TYPELIST1(T2) >
#define TYPELIST3(T1, T2, T3) TypeList<T1, TYPELIST2(T2, T3) >
#define TYPELIST4(T1, T2, T3, T4) TypeList<T1, TYPELIST3(T2, T3, T4) >

namespace TL
{
  // length function
  template<class TL> struct Length;
  
  template<> 
  struct Length <NullType> 
  {
    enum { value = 0 };
  };

  template<class Head, class Tail> 
  struct Length < TypeList<Head, Tail> >
  {
    enum { value = 1 + Length<Tail>::value };
  };

  // item at 
  template <class TL, unsigned int> struct TypeAt;

  template <class Head, class Tail>
  struct TypeAt < TypeList<Head, Tail>, 0 >
  {
    typedef Head Result;
  };

  template <class Head, class Tail, unsigned int n>  
  struct TypeAt < TypeList<Head, Tail>, n >
  {
    typedef typename TypeAt<Tail, n - 1>::Result Result;
  };

  // index of
  template <class T, class TL> struct IndexOf;  

  template <class T> 
  struct IndexOf<T, NullType>
  {
    enum { value = -1 };
  };

  template <class T, class Tail>
  struct IndexOf< T, TypeList<T, Tail> >
  {
    enum { value = 0 };
  };

  template <class T, class Head, class Tail>
  class IndexOf < T, TypeList<Head, Tail> >
  {
    enum { temp = IndexOf<T, TypeList<Head, Tail> >::value };
  public:
    enum { value = temp == -1 ? -1 : 1 + temp };
  };

  // append
  template <class T, class TL> struct Append;

  template <>
  struct Append<NullType, NullType> 
  {
    typedef NullType Result;
  };

  template <class T>
  struct Append<T, NullType>
  {
    typedef TYPELIST1(T) Result;
  };

  template <class T, class Head, class Tail>
  struct Append<T, TypeList<Head, Tail> >
  {
    typedef typename TypeList<Head, Append<T, Tail> >::Result Result;
  };

  // erase
  template <class T, class TL> struct Erase;

  template <class T>
  struct Erase<T, NullType>
  {
    typedef NullType Result;
  };

  template <class T, class Tail>
  struct Erase<T, TypeList<T, Tail> >
  {
    typedef Tail Result;
  };

  template <class T, class Head, class Tail>
  struct Erase<T, TypeList<Head, Tail> >
  {
    typedef typename TypeList<Head, Erase<T, Tail> >::Result Result;
  };
}

#endif /* _TYPELIST_HPP_ */
