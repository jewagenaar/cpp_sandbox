#ifndef _CHECKER_HPP_
#define _CHECKER_HPP_

template<class T>
struct NullChecker
{
  struct NullPointerException { /* ... */ };

  static void Check(T* obj)
  {
    if (obj == 0) {
      std::cout << "null pointer exception" << std::endl; 
      throw NullPointerException();
    }
  }
};

template<class T>
struct PassChecker
{
  static void Check(T* obj) { /* ... */ }
};

#endif /* _CHECKER_HPP_ */
