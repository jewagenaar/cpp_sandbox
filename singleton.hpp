#ifndef _SINGLETON_HPP_
#define _SINGLETON_HPP_

template<class T>
struct SingletonHolder
{
  static T* instance()
  {
    static T _instance;
    return &_instance;
  }
};

#endif //_SINGLETON_HPP_
