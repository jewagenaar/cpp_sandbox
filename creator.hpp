#ifndef _CREATOR_HPP_
#define _CREATOR_HPP_

#include <string.h>
#include <stdlib.h>
#include <iostream>

template <class T>
struct SimpleCreator
{
  static T* Create (void)
  {
    return new T;
  }

  static void Destroy (T *obj) 
  {
    delete obj;
  }
};

template <class T>
struct MallocCreator
{
  static T* Create (void)
  {
    void *chunk = malloc(sizeof(T));
    if (!chunk) {
      return 0;
    }
    return new (chunk) T;
  }

  static void Destroy (T *obj) 
  {
    free (obj);
  }
};

template <class T>
struct CacheCreator
{ 
  static const int N = 100;

  static T    m_Cache [N];
  static bool m_Index [N];

  static void Init (void)
  {
    memset(m_Index, false, N * sizeof(bool));
  }

  static T* Create (void)
  {
    int i = 0;
    // scan for available index
    while (i < N) {
      if (m_Index[i]) {
	i++;
      } else {
	break;
      }
    }

    if (i < N) {
      m_Index[i] = true;
      return new (&m_Cache[i]) T;
    } else {
      return 0;      
    }
  }

  static void Destroy (T *obj)
  {
    int i = 0;
    while (i < N) {
      if (obj == &m_Cache[i]) {
	std::cout << " delete!" << std::endl;
	m_Index[i] = false;
	return;
      } else {
	i++;
      }
    } 
    std::cout << std::endl;
  }
};

template<class T>
T CacheCreator<T>::m_Cache[CacheCreator::N];

template<class T>
bool CacheCreator<T>::m_Index[CacheCreator::N];

#endif
