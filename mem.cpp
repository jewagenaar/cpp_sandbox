#include "mem.hpp"
#include "singleton.hpp"
#include <stdlib.h>

// CHUNK

/**
 * Initializes the chunk. It will allocate the chunk 
 * and initialize the embedded list.
 */
void Chunk::Init(size_t blocksize, unsigned char blocks)
{
  data = new unsigned char [blocksize * blocks];
  
  firstAvailable = 0;
  numAvailable   = blocks;

  int i = 0;
  unsigned char *p = data;

  for (; i != blocks; p += blocksize) {
    *p = ++i;
  }
}

/**
 * Allocates space in the chunk.
 */
void* Chunk::Alloc(size_t blocksize)
{
  if (!numAvailable) return 0;

  unsigned char* alloc = data + (firstAvailable * blocksize);
  firstAvailable = *alloc;
  numAvailable--;
  
  return alloc;
}

/**
 * Frees up the memory in the chunk and marks it as available.
 */
void Chunk::Dealloc(void *p, size_t blockSize)
{
  unsigned char *dealloc = static_cast<unsigned char*>(p);

  *dealloc       = firstAvailable;
  firstAvailable = (dealloc - data) / blockSize;

  numAvailable++;
}

// HELPER FUNCTIONs

/**
 * Inline function to check if a memory address is within the bounds of the Chunk.
 */
inline bool ChunkContains (Chunk *c, void *p, size_t blocksize, unsigned char blocks)
{
  return (c->data <= p) && (p < c->data + blocksize*blocks);
}

// FIXED ALLOCATOR

/**
 * 
 */
FixedAllocator::FixedAllocator(size_t blocksize, unsigned char blocks) :
  m_Blocksize   (blocksize),
  m_Blocks      (blocks),
  m_LastAlloc   (0),
  m_LastDealloc (0)
{
  /* ... */
}

/**
 * Allocates a block of memoy. It will create new chunks as needed and
 * will look for space in the last allocated chunk first.
 */
void* FixedAllocator::Alloc(void)
{
  if (m_LastAlloc == 0 ||
      m_LastAlloc->numAvailable == 0) {

      Chunks::iterator i = m_Chunks.begin();      
      for(;; ++i) {
	// no available chunk
	if (i == m_Chunks.end()) {

	  m_Chunks.reserve(m_Chunks.size() + 1);
	  Chunk c;
	  c.Init(m_Blocksize, m_Blocks);
	  m_Chunks.push_back(c);
	  
	  m_LastAlloc   = &m_Chunks.back();;
	  m_LastDealloc = &m_Chunks.back();
	  break;
	}
	
	// check the next chunk
	if (i->numAvailable > 0) {
	  m_LastAlloc = &*i;
	  break;
	}
      }
    }

  // do some asserts on our chunk
  assert(m_LastAlloc);
  assert(m_LastAlloc->numAvailable);

  // found a chunk - return it
  return m_LastAlloc->Alloc(m_Blocksize);
}

/**
 * Deallocatos the memory that is pointed to be p. Traverses the chunks list
 * in two directions starting from the top and bottom of the chunks list.
 * TODO: change so that it starts from the point of last allocation.
 */
void FixedAllocator::Dealloc(void *p)
{
  // check if in last dealloc chunk
  if (ChunkContains(m_LastDealloc, p, m_Blocksize, m_Blocks)) {
    m_LastDealloc->Dealloc(p, m_Blocksize);
    return;
  }

  // go look for Chunk that contains this object
  Chunks::iterator fwd = m_Chunks.begin();
  Chunks::iterator bck = m_Chunks.end();

  while (++fwd >= --bck) {
    // check if in fwd chunks
    if (ChunkContains(&*fwd, p, m_Blocksize, m_Blocks)) {
      m_LastDealloc = &*fwd;
      m_LastDealloc->Dealloc(p, m_Blocksize);
      break;
    }
    
    // check if in bck chunks
    if (ChunkContains(&*bck, p, m_Blocksize, m_Blocks)) {
      m_LastDealloc = &*bck;
      m_LastDealloc->Dealloc(p, m_Blocksize);
      break;
    }
  }

  // drop empty chunks chunks - if more than two
  if ((m_LastDealloc->numAvailable == m_Blocks) && 
      (2 == ++m_EmptyChunks)) {
      
    std::vector<Chunk>::iterator iter = m_Chunks.begin();
    for (; iter != m_Chunks.end(); iter++) {
      Chunk curr = *iter;
      if (curr.numAvailable == m_Blocks) {
	m_Chunks.erase(iter);
	m_EmptyChunks--;
	break;
      }
    }    
  }
}

// SMALL OBJECT ALLOCATOR

/**
 * The constructor for a small object allocator takes the maximum size of small objects
 * and the number of blocks in a chunk as parameters.
 */
SmallObjectAllocator::SmallObjectAllocator(size_t maxSize, unsigned char blocks) :
  m_MaxSize(maxSize),
  m_Blocks (blocks)
{
  /* ... */
}

/**
 * Allocator memory of size s. If s is bigger than size s it
 * will allocate using malloc, if size s can be allocated using
 * an fixed size allocator it will be used (allocators are created
 * as needed). 
 */
void* SmallObjectAllocator::Alloc (size_t s) 
{
  void *result = 0;

  if (s > m_MaxSize) {
    // s is bigger than our maxsize
    result = (void *) malloc(s);
  } else {
    // see if we can find an allotar for s in the pool
    FixedAllocator *alloc = 0;    
    std::map<size_t, FixedAllocator *>::iterator f = m_Pool.find(s);
    
    if (f == m_Pool.end()) {
      // no such allocator - create a new one
      alloc = new FixedAllocator(s, m_Blocks);
      m_Pool[s] = alloc;
    } else {
      // found it
      alloc = m_Pool[s];
    }
    
    assert(alloc);
    result = alloc->Alloc();
  }

  assert(result);
  return result;
}

/**
 * Deallocate the given pointer of size s. If no s is larger
 * than the size that can be handled by the allocator, then
 * it assumed that it was allocated using malloc and free is used.
 * Otherwise the allocator that allocates elements of size s is
 * located and used to deallocate the memory.
 */
void SmallObjectAllocator::Dealloc(void *p, size_t s)
{
  if (s > m_MaxSize) {
    free(p);
    p = 0;
  } else {
    FixedAllocator *dealloc = 0;
    std::map<size_t, FixedAllocator *>::iterator f = m_Pool.find(s);
    
    if(f != m_Pool.end()) {
      FixedAllocator *dealloc = m_Pool[s];
      dealloc->Dealloc(p);
      p = 0;
    }
  }

  assert(p == 0);
}

// SMALL OBJECT

/**
 * SmallObject new operator will delegate memory allocation
 * to the SmallObjectAllocator instance.
 */
void* SmallObject::operator new (size_t s)
{
  return SingletonHolder<SmallObjectAllocator>::instance()->Alloc(s);
}

/**
 * SmallObject delete operator will delegate the memory deallocation
 * to the SmallObjectAllocator instance.
 */
void SmallObject::operator delete (void *p, size_t s)
{
  SingletonHolder<SmallObjectAllocator>::instance()->Dealloc(p, s);
}

int main()
{
  return 0;
}
