#include <iostream>
#include <stdlib.h>
#include <vector>

#include "creator.hpp"
#include "checker.hpp"
#include "types.hpp"
#include "typelist.hpp"

/* a dummy object for testing policies */
class DummyObject
{
  int value;

public:
  DummyObject()
  {
    static int _v = 0;
    value = _v++;
  }

  void Print()
  {
    std::cout << this << "::" << value << std::endl;
  }
};

struct CompositeObject : public DummyObject
{
  int childCount;
  DummyObject *children;
};

/* template definition for an object manager */
template <
class T, 
template <class> class Checker,
template <class> class Creator>
struct ObjectManager : public Creator<T>
{
  static T* New()
  {
    T *obj = Creator<T>::Create();
    Checker<T>::Check(obj);
    return obj;
  }
};

typedef ObjectManager<DummyObject, NullChecker, CacheCreator> Factory;
typedef NullChecker<DummyObject>::NullPointerException NullPointerException;

int main (int argc, char **argv) 
{ 
  int l = TL::Length<TYPELIST2(int, float)>().value;

  TL::TypeAt<TYPELIST2(int, float), 1>::Result v = 2.01f;

  std::cout << "V value  : " << v << std::endl;
  std::cout << "TL length: " << l << std::endl;

  Factory::Init();

  std::cout << CanConvert<DummyObject, CompositeObject>::value << ' '
	    << CanConvert<CompositeObject, DummyObject>::value << std::endl;

  DummyObject *o = new DummyObject;
  DummyObject *c = CopyPolicy<DummyObject, false>::Copy(o);

  typedef DummyObject* object_t;

  std::cout << "std::vector<int>::iterator isPointer: " <<
    TypeInfo<std::vector<int>::iterator>().isPointer << std::endl;

  std::cout << "object_t isPointer: " <<
    TypeInfo<object_t>().isPointer << std::endl;

  for (int i = 0; i < 5; i++) {
    try {
      DummyObject *obj = Factory::New();
      if (obj) {
	obj->Print();
	
	if(rand() % 2 == 0) {
	  Factory::Destroy(obj);
	}
      }
    } catch (NullPointerException x) {
	std::cout << "null pointer exception" << std::endl;
    }
  }
  
  return 0;
}
